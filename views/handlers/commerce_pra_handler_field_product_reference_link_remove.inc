<?php
/**
 * @file
 * Field handler to present a link to remove a product reference.
 */

class commerce_pra_handler_field_product_reference_link_remove extends views_handler_field {

  public function construct() {
    parent::construct();
    $this->additional_fields['product_id'] = 'product_id';
  }

  public function option_definition() {
    $options = parent::option_definition();

    $options['text'] = array('default' => '', 'translatable' => TRUE);

    return $options;
  }

  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text to display'),
      '#default_value' => $this->options['text'],
    );
  }

  public function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {

    // Check there is a nid argument.
    if (!isset($this->view->argument['nid'])) {
      return;
    }

    if (!isset($values->nid)) {
      return;
    }

    $nid = $values->nid;

    // Get product ID from query.
    $product_id = $this->get_value($values, 'product_id');

    // Ensure the user has access to remove this product reference.
    $node = node_load($nid);
    if (!commerce_pra_access('remove', $node)) {
      return;
    }

    $text = !empty($this->options['text']) ? $this->options['text'] : t('remove reference');

    return l($text, 'node/' . $nid . '/product-admin/remove/' . $product_id, array('query' => drupal_get_destination()));
  }
}
